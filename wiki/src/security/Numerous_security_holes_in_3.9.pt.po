# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2018-10-04 11:55+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Tue, 2 Oct 2018 12:34:56 +0000\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Numerous security holes in Tails 3.9\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!tag security/fixed]]\n"
msgstr "[[!tag security/fixed]]\n"

#. type: Plain text
msgid "Several security holes that affect Tails 3.9 are now fixed in Tails 3.9.1."
msgstr ""

#. type: Plain text
msgid ""
"We **strongly** encourage you to [[upgrade to "
"Tails 3.9.1|news/version_3.9.1]] as soon as possible."
msgstr ""

#. type: Bullet: ' - '
msgid "Tor Browser: [[!mfsa 2018-22]]"
msgstr ""

#. type: Bullet: ' - '
msgid "Thunderbird: [[!mfsa 2018-22]]"
msgstr ""

#. type: Bullet: ' - '
msgid "lcms2: [[!debsa2018 4284]]"
msgstr ""

#. type: Bullet: ' - '
msgid "curl: [[!debsa2018 4286]]"
msgstr ""

#. type: Bullet: ' - '
msgid "ghostscript: [[!debsa2018 4288]], [[!debsa2018 4294]]"
msgstr ""

#. type: Bullet: ' - '
msgid "texlive-bin: [[!debsa2018 4299]]"
msgstr ""

#. type: Bullet: ' - '
msgid "python2.7: [[!debsa2018 4306]]"
msgstr ""

#. type: Bullet: ' - '
msgid "python3.5: [[!debsa2018 4307]]"
msgstr ""
