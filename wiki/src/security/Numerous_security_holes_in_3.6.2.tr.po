# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2018-10-01 16:15+0000\n"
"PO-Revision-Date: 2018-06-27 23:33+0000\n"
"Last-Translator: Weblate Admin <admin@example.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 2.10.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Wed, 09 May 2018 17:25:18 +0000\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Numerous security holes in Tails 3.6.2\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!tag security/fixed]]\n"
msgstr "[[!tag security/fixed]]\n"

#. type: Plain text
msgid ""
"Several security holes that affect Tails 3.6.2 are now fixed in Tails 3.7."
msgstr ""

#. type: Plain text
msgid ""
"We **strongly** encourage you to [[upgrade to Tails 3.7|news/version_3.7]] "
"as soon as possible."
msgstr ""

#. type: Bullet: ' - '
msgid ""
"Tor Browser: [MSFA 2018-12](https://www.mozilla.org/en-US/security/"
"advisories/mfsa2018-12/)"
msgstr ""

#. type: Bullet: ' - '
msgid "OpenSSL: [[!debsa2018 4157]]"
msgstr ""

#. type: Bullet: ' - '
msgid "Perl: [[!debsa2018 4172]]"
msgstr ""

#. type: Bullet: ' - '
msgid "Libre Office: [[!debsa2018 4178]]"
msgstr ""

#. type: Bullet: ' - '
msgid "libmad: [[!debsa2018 4192]]"
msgstr ""
