# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: Tails l10n\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2017-06-07 17:03+0000\n"
"PO-Revision-Date: 2018-11-02 17:26+0000\n"
"Last-Translator: Weblate Admin <admin@example.com>\n"
"Language-Team: Tails Chinese translators <jxt@twngo.xyz>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.19.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"About\"]]\n"
msgstr "[[!meta title=\"關於Tails\"]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"> **amnesia**, *noun*:<br/>\n"
"> forgetfulness; loss of long-term memory.\n"
msgstr ""
"> **失憶**, *名詞*:<br/>\n"
"> 遺忘; 失去長期記憶.\n"

#. type: Plain text
#, no-wrap
msgid ""
"> **incognito**, *adjective & adverb*:<br/>\n"
"> (of a person) having one's true identity concealed.\n"
msgstr ""
"> **隱身**, *形容詞*:<br/>\n"
">一個人隱藏起自己真正的身份。\n"

#. type: Plain text
msgid ""
"Tails is a live system that aims to preserve your privacy and anonymity. It "
"helps you to use the Internet anonymously and circumvent censorship almost "
"anywhere you go and on any computer but leaving no trace unless you ask it "
"to explicitly."
msgstr ""
"Tails是一個自生系統，其目的是為了保護用戶的隱私和匿名。除非使用者刻意更改設定，否則它可以協助用戶隨處使用任何一部電腦匿名上網和躲避審查，且不會留下㾗"
"跡。"

#. type: Plain text
msgid ""
"It is a complete operating system designed to be used from a USB stick or a "
"DVD independently of the computer's original operating system. It is [[Free "
"Software|doc/about/license]] and based on [[Debian "
"GNU/Linux|https://www.debian.org/]]."
msgstr ""

#. type: Plain text
msgid ""
"Tails comes with several built-in applications pre-configured with security "
"in mind: web browser, instant messaging client, email client, office suite, "
"image and sound editor, etc."
msgstr "Tails 包含了一些應用程式，其原始設定皆以安全為優先，如：網頁瀏覽器、即時通訊軟體、電子郵件軟體、辦公室軟體、圖片和音效編輯軟體等。"

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=2]]\n"
msgstr "[[!toc levels=2]]\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"tor\"></a>\n"
msgstr "<a id=\"tor\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Online anonymity and censorship circumvention\n"
msgstr "線上匿名與審查規避\n"

#. type: Plain text
#, no-wrap
msgid ""
"Tor\n"
"---\n"
msgstr ""
"Tor\n"
"---\n"

#. type: Plain text
msgid "Tails relies on the Tor anonymity network to protect your privacy online:"
msgstr "Tails 依靠 Tor 的匿名網路來保護用戶線上隱私安全:"

#. type: Bullet: '  - '
msgid "all software is configured to connect to the Internet through Tor"
msgstr "所有軟體已設定要透過 Tor 連上互聯網"

#. type: Bullet: '  - '
msgid ""
"if an application tries to connect to the Internet directly, the connection "
"is automatically blocked for security."
msgstr "如何某個應用程式想要直接地連上互聯網，此連線會因安全考量而自動地封鎖掉。"

#. type: Plain text
msgid ""
"Tor is an open and distributed network that helps defend against traffic "
"analysis, a form of network surveillance that threatens personal freedom and "
"privacy, confidential business activities and relationships, and state "
"security."
msgstr "Tor　是一個開放分散的網路，協助抵禦流量分析，它是一種網路監控的方式，其威脅了個人的自由和隱私、防礙機密的商業活動、關係和國家安全。"

#. type: Plain text
msgid ""
"Tor protects you by bouncing your communications around a network of relays "
"run by volunteers all around the world: it prevents somebody watching your "
"Internet connection from learning what sites you visit, and it prevents the "
"sites you visit from learning your physical location."
msgstr ""
"Tor 利用全球各地志願者所運行的中繼站跳接來傳輸網路通訊：它可預防有人透過知道用戶訪問了哪些網站來進行監視，也防止所訪問過的網站會知道用戶的實際位置。"

#. type: Plain text
msgid "Using Tor you can:"
msgstr "使用Tor可讓你:"

#. type: Bullet: '  - '
msgid "be anonymous online by hiding your location,"
msgstr "透過隱藏用戶位置以實現線上匿名"

#. type: Bullet: '  - '
msgid "connect to services that would be censored otherwise;"
msgstr "連結上可能已被審查禁止的服務"

#. type: Bullet: '  - '
msgid ""
"resist attacks that block the usage of Tor using circumvention tools such as "
"[[bridges|doc/first_steps/startup_options/bridge_mode]]."
msgstr ""
"使用Tor作為規避工具來抵抗封鎖攻擊，例如[[Tor-橋接|doc/first_steps/startup_options/bridge_mode]] 。"

#. type: Plain text
msgid ""
"To learn more about Tor, see the official [Tor "
"website](https://www.torproject.org/), particularly the following pages:"
msgstr "進一步了解 Tor，請到 [ Tor 官網](https://www.torproject.org), 尤其是參考以下頁面："

#. type: Bullet: '- '
msgid ""
"[Tor overview: Why we need "
"Tor](https://www.torproject.org/about/overview.html.en#whyweneedtor)"
msgstr ""
"[ Tor 瀏覽器總覽：為何需使用 Tor "
"](https://www.torproject.org/about/overview.html.en#whyweneedtor) (auf "
"Englisch)"

#. type: Bullet: '- '
msgid ""
"[Tor overview: How does Tor "
"work](https://www.torproject.org/about/overview.html.en#thesolution)"
msgstr ""
"[ Tor "
"瀏覽器總覽：它的運作原理](https://www.torproject.org/about/overview.html.en#thesolution)"

#. type: Bullet: '- '
msgid "[Who uses Tor?](https://www.torproject.org/about/torusers.html.en)"
msgstr "哪些人使用 Tor 瀏覽器](https://www.torproject.org/about/torusers.html.en)"

#. type: Bullet: '- '
msgid ""
"[Understanding and Using Tor — An Introduction for the "
"Layman](https://trac.torproject.org/projects/tor/wiki/doc/TorALaymansGuide)"
msgstr ""
"[了解和使用Tor— 給外行人的入門介紹](https://trac.torproject.org/projects/tor/wiki/doc/TorAL"
"aymansGuide) (英文)"

#. type: Plain text
msgid ""
"To learn more about how Tails ensures all its network connections use Tor, "
"see our [[design document|contribute/design/Tor_enforcement]]."
msgstr ""
"進一步了解 Tails　如何確保其透過 Tor 連結互聯網，請見 [[設計文件|contribute/design/Tor_enforcement]]."

#. type: Plain text
#, no-wrap
msgid "<a id=\"amnesia\"></a>\n"
msgstr "<a id=\"amnesia\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Use anywhere but leave no trace\n"
msgstr "可隨處使用而不留下㾗跡\n"

#. type: Plain text
msgid ""
"Using Tails on a computer doesn't alter or depend on the operating system "
"installed on it. So you can use it in the same way on your computer, a "
"friend's computer, or one at your local library. After shutting down Tails, "
"the computer will start again with its usual operating system."
msgstr ""
"在任何一部電腦上使用 Tails　並不會改變或得依賴其原本安裝的作業系統。同樣地，用戶也可以在自己的電腦、朋友的電腦或是圖書館裏的公用電腦使用Tails。"
"關閉 Tails　之後，電腦就會自動以原本的作業系統開機使用。"

#. type: Plain text
msgid ""
"Tails is configured with special care to not use the computer's hard-disks, "
"even if there is some swap space on them. The only storage space used by "
"Tails is in RAM, which is automatically erased when the computer shuts "
"down. So you won't leave any trace on the computer either of the Tails "
"system itself or what you used it for. That's why we call Tails \"amnesic\"."
msgstr ""
"Tails 透過特別小心的設定以不會使用到電腦的硬碟，即便有人刻意想要交換二者的空間。Tails唯一會使用到的儲存空間是快取記憶體 (RAM), "
"當電腦關機時它就會自動地消除。因此用戶不會在電腦上留下任何Tails 的使用㾗跡。這就是為何我們稱Tails為 \"失憶\" (amnesic)."

#. type: Plain text
msgid ""
"This allows you to work with sensitive documents on any computer and "
"protects you from data recovery after shutdown. Of course, you can still "
"explicitly save specific documents to another USB stick or external "
"hard-disk and take them away for future use."
msgstr ""
"這讓人可以在任何一台電腦上使用敏感的文件預防避免關機後資料的恢復。當然，用戶仍然可將文件儲存在另一支USB隨身碟或外接式硬磁上，以隨時攜帶它們以備未來之需"
"。"

#. type: Plain text
#, no-wrap
msgid "<a id=\"cryptography\"></a>\n"
msgstr "<a id=\"cryptography\"></a>\n"

#. type: Title =
#, no-wrap
msgid "State-of-the-art cryptographic tools\n"
msgstr "最先進的密碼學工具\n"

#. type: Plain text
msgid ""
"Tails also comes with a selection of tools to protect your data using strong "
"encryption:"
msgstr "Tails 也預載了一些工具選項以利用強大的加密功能來保護用戶的資料:"

#. type: Bullet: '  - '
msgid ""
"[[Encrypt your USB sticks or external "
"hard-disks|doc/encryption_and_privacy/encrypted_volumes]] using <span "
"class=\"definition\">[[!wikipedia Linux_Unified_Key_Setup "
"desc=\"LUKS\"]]</span>, the Linux standard for disk-encryption."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"Automatically use HTTPS to encrypt all your communications to a number of "
"major websites using [HTTPS "
"Everywhere](https://www.eff.org/https-everywhere), a Firefox extension "
"developed by the [Electronic Frontier Foundation](https://www.eff.org)."
msgstr ""
"自動地採用[HTTPS Everywhere](https://www.eff.org/https-everywhere)來加密上網瀏覽的通訊. "
"HTTPS Everywhere 是Firefox的外加元件，其由[電子前鋒基金會 Electronic Frontier "
"Foundation](https://www.eff.org) 所開發。"

#. type: Bullet: '  - '
msgid ""
"Encrypt and sign your emails and documents using the *de facto* standard "
"<span class=\"definition\">[[!wikipedia Pretty_Good_Privacy#OpenPGP "
"desc=\"OpenPGP\"]]</span> either from Tails email client, text editor or "
"file browser."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"Protect your instant messaging conversations using <span "
"class=\"definition\">[[!wikipedia Off-the-Record_Messaging "
"desc=\"OTR\"]]</span>, a cryptographic tool that provides encryption, "
"authentication and deniability."
msgstr ""
"保護網路即時通訊的交談內容，請使用 <span class=\"definition\">[[!wikipedia_de Off-the-"
"Record_Messaging desc=\"OTR\"]]</span> -它是一種密碼學工具可以用來加密、認證與否決。"

#. type: Bullet: '  - '
msgid ""
"[[Securely delete your files|doc/encryption_and_privacy/secure_deletion]] "
"and clean your diskspace using [[Nautilus "
"Wipe|http://wipetools.tuxfamily.org/nautilus-wipe.html]]."
msgstr ""
"[[安全地刪除你的檔案|doc/encryption_and_privacy/secure_deletion]] 以及利用[[Nautilus "
"Wipe|http://wipetools.tuxfamily.org/nautilus-wipe.html]]來清理磁碟 ."

#. type: Plain text
msgid ""
"[[Read more about those tools in the "
"documentation.|doc/encryption_and_privacy]]"
msgstr "[[從文檔中進一步了解這些工具.|doc/encryption_and_privacy]]"

#. type: Title =
#, no-wrap
msgid "What's next?\n"
msgstr "接下來是什麼?\n"

#. type: Plain text
msgid "To continue discovering Tails, you can now read:"
msgstr "繼續探索Tails的好用之處, 你可以讀讀:"

#. type: Bullet: '  - '
msgid ""
"the [[warning page|doc/about/warning]] to better understand the security "
"limitations of Tails and Tor,"
msgstr "這個 [[警告頁|doc/about/warning]], 可讓人更了解Tails與Tor的安全限制,"

#. type: Bullet: '  - '
msgid ""
"more details about the [[features and software|doc/about/features]] included "
"in Tails,"
msgstr "更多詳細的[[特色和軟體|doc/about/features]], 其已內鍵於Tails之中,"

#. type: Bullet: '  - '
msgid "our [[installation instructions|install]] to download and install Tails,"
msgstr "我們有 [[安裝指示|install]] 來下載和安裝Tails,"

#. type: Bullet: '  - '
msgid "our [[documentation|doc]] explaining in detail how to use Tails,"
msgstr "我們的 [[文件檔案|doc]], 詳細地說明了如何使用Tails,"

#. type: Bullet: '  - '
msgid "some hints on why [[you should trust Tails|doc/about/trust]],"
msgstr "有些小提示關於為何 [[你應該使用Tails|doc/about/trust]],"

#. type: Bullet: '  - '
msgid ""
"our [[design document|contribute/design]] laying out Tails specification, "
"threat model and implementation,"
msgstr "我們的 [[設計文檔|contribute/design]]介紹了Tails的特殊性、威脅模型與執行。"

#. type: Bullet: '  - '
msgid ""
"the [[calendar|contribute/calendar]] that holds our release dates, meetings "
"and other events."
msgstr "[[行事曆|contribute/calendar]]上有新版本發佈日期、會議與其它活動宣佈。"

#. type: Title =
#, no-wrap
msgid "Press and media\n"
msgstr "新聞與媒體\n"

#. type: Plain text
msgid "See [[Press and media information|press]]."
msgstr "請見[[新聞與媒體資訊|press]]."

#. type: Title =
#, no-wrap
msgid "Social Contract\n"
msgstr "社會契約\n"

#. type: Plain text
msgid "Read our [[Social Contract|contribute/working_together/social_contract]]."
msgstr "諘看我們的 [[社會契ˋ約|contribute/working_together/social_contract]]."

#. type: Title =
#, no-wrap
msgid "Acknowledgments and similar projects\n"
msgstr "致謝與相類似的專案\n"

#. type: Plain text
msgid ""
"See [[Acknowledgments and similar "
"projects|doc/about/acknowledgments_and_similar_projects]]."
msgstr "請見 [[致謝與相類似專案|doc/about/acknowledgments_and_similar_projects]]."

#. type: Title =
#, no-wrap
msgid "Contact\n"
msgstr "聯絡\n"

#. type: Plain text
msgid "See the [[contact page|about/contact]]."
msgstr "請參見[[聯絡頁面|about/contact]]."
