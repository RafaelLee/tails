# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2018-10-23 19:50+0000\n"
"PO-Revision-Date: 2018-07-02 10:47+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Weblate 2.10.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Finances\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=2]]\n"
msgstr "[[!toc levels=2]]\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"partners\"></a>\n"
msgstr "<a id=\"partners\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Partners\n"
msgstr ""

#. type: Plain text
msgid ""
"Grants, awards, corporate donations, and substantial donations from "
"individuals are listed in a more visual way on our [[partners page|"
"partners]]."
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "<a id=\"2016\"></a>\n"
msgid "<a id=\"2017\"></a>\n"
msgstr "<a id=\"2016\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Income statement for 2017\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\tRevenues:\n"
"\t- Donations\n"
"\t           $23579.15   via RiseupLabs\n"
"\t             8246.82€  via CCT\n"
"\t             2045.81€  cash\n"
"\t               24.33Ƀ  bitcoins\n"
"\t- Partners:\n"
"\t            $1000.00   ExpressVPN\n"
"\t                0.10Ƀ  I2P\n"
"\t- Restricted funds (sponsor deliverables)\n"
"\t            11000.00€  Lush\n"
"\t- T-shirts\n"
"\t             1272.35€  T-shirts\n"
"\t--------------------\n"
"\t           $24579.15\n"
"\t               24.43Ƀ\n"
"\t            22564.98€\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\tExpenses:\n"
"\t- Infrastructure\n"
"\t            -2706.56€  hardware\n"
"\t           $-1586.97   hardware\n"
"\t             -544.32€  Internet hosting\n"
"\t           $-4380.00   Internet hosting\n"
"\t- Travel & meetings\n"
"\t            -9449.31€  travel\n"
"\t           $-1720.26   travel\n"
"\t            -4388.97€  food and drinks\n"
"\t              $41.58   food and drinks\n"
"\t            $-382.07   hosting\n"
"\t            -2032.33€  hosting\n"
"\t             -586.61€  tickets\n"
"\t              -44.95€  misc\n"
"\t- Subcontracting\n"
"\t          -108079.77€  subcontracting\n"
"\t           $-2205.00   subcontracting\n"
"\t- Running costs\n"
"\t          $-13000.96   administration\n"
"\t            -3672.65€  administration\n"
"\t           $-3901.12   banking\n"
"\t               -0.18Ƀ  banking\n"
"\t              -18.00€  banking\n"
"\t- Misc\n"
"\t            -3279.64€  T-shirts\n"
"\t             -163.57€  office\n"
"\t- Taxes\n"
"\t           -19334.81€  VAT\n"
"\t--------------------\n"
"\t          $-27217.96\n"
"\t               -0.18Ƀ\n"
"\t          -154322.32€\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\tTotal:\n"
"\t--------------------\n"
"\t          -131757.34€\n"
"\t           $-2638.81\n"
"\t               24.26Ƀ\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"2016\"></a>\n"
msgstr "<a id=\"2016\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Income statement for 2016\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\tRevenues:\n"
"\t- Donations\n"
"\t           $21908.52   via RiseupLabs\n"
"\t            $7586.00   via Freedom of the Press Foundation\n"
"\t            13709.76€  via Zwiebelfreunde\n"
"\t              784.00€  cash\n"
"\t               72.35Ƀ  bitcoins\n"
"\t- Restricted funds (sponsor deliverables)\n"
"\t          $208000.00   Open Technology Fund\n"
"\t           $77000.00   Mozilla Open Source Support\n"
"\t            $5000.00   Google Summer of Code\n"
"\t- Partners\n"
"\t             2000.00€  Mediapart\n"
"\t- T-shirts\n"
"\t              180.47€  T-shirts\n"
"\t--------------------\n"
"\t          $319494.52\n"
"\t               72.35Ƀ\n"
"\t            16674.23€\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\tExpenses:\n"
"\t- Infrastructure\n"
"\t           $-2351.70   hardware\n"
"\t            -2081.40€  hardware\n"
"\t             -900.00€  Internet hosting\n"
"\t- Travel & meetings\n"
"\t            -7566.00€  travel\n"
"\t            $-526.74   travel\n"
"\t            -3789.61€  food and drinks\n"
"\t             $-25.75   food and drinks\n"
"\t            -2577.00€  hosting\n"
"\t- Subcontracting\n"
"\t          -128135.54€  subcontracting\n"
"\t           $-5945.00   subcontracting\n"
"\t- Running costs\n"
"\t            -8038.03€  administration\n"
"\t           $-5324.73   administration\n"
"\t              -21.00€  banking\n"
"\t            $-170.00   banking\n"
"\t- Misc\n"
"\t             -115.43€  non stored purchases\n"
"\t              -30.00€  communication\n"
"\t- Taxes\n"
"\t           -12034.02€  VAT\n"
"\t          $-37544.00   income taxes\n"
"\t--------------------\n"
"\t          $-51887.92\n"
"\t          -165288.03€\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\tTotal:\n"
"\t--------------------\n"
"\t          $268406.60\n"
"\t               72.35Ƀ\n"
"\t          -148613.80€\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"2015\"></a>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Income statement for 2015\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\tRevenues:\n"
"\t- Donations\n"
"\t           $35000.00   via RiseupLabs\n"
"\t            $8599.00   via Freedom of the Press Foundation\n"
"\t             6995.74€  via Zwiebelfreunde\n"
"\t             $ 47.70   cash\n"
"\t              557.05€  cash\n"
"\t               31.00Ƀ  bitcoins\n"
"\t- Restricted funds (sponsor deliverables)\n"
"\t            70000.00€  Hivos\n"
"\t- Misc\n"
"\t            $1000.00   selling old hardware\n"
"\t--------------------\n"
"\t           $44646.70\n"
"\t               31.00Ƀ\n"
"\t            77552.79€\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\tExpenses:\n"
"\t- Infrastructure\n"
"\t            $-787.23   hardware\n"
"\t             -775.41€  hardware\n"
"\t             -900.00€  Internet hosting\n"
"\t            $-160.00   SSL certificates\n"
"\t- Travel & meetings\n"
"\t            -9279.73€  travel\n"
"\t            -8155.21€  food and drinks\n"
"\t             -988.00€  hosting\n"
"\t- Subcontracting\n"
"\t          -146365.22€  subcontracting\n"
"\t           $-4860.00   subcontracting\n"
"\t- Running costs\n"
"\t           $-2179.95   administration\n"
"\t            -3849.79€  administration\n"
"\t             $-85.00   banking\n"
"\t             -106.41€  banking\n"
"\t- Misc\n"
"\t            -4151.64€  lost bitcoins\n"
"\t               -8.85Ƀ  lost bitcoins\n"
"\t            $-702.77   income taxes\n"
"\t              -39.90€  promotion material\n"
"\t--------------------\n"
"\t           $-8774.95\n"
"\t          -174611.30€\n"
"\t               -8.85Ƀ  lost bitcoins\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\tTotal:\n"
"\t--------------------\n"
"\t           $35871.75\n"
"\t           -97058.51€\n"
"\t               22.15Ƀ\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"2014\"></a>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Income statement for 2014\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\tRevenues:\n"
"\t- Donations\n"
"\t           $33376.83   via Freedom of the Press Foundation\n"
"\t               71.67Ƀ  via bitcoin\n"
"\t             7150.19€  via Zwiebelfreunde\n"
"\t              803.50€  misc\n"
"\t- Restricted funds (sponsor deliverables)\n"
"\t           $50000.00   Access Now\n"
"\t             5000.00€  FFIS\n"
"\t            70000.00€  Hivos\n"
"\t           $34884.00   NDI\n"
"\t           $25800.00   OpenITP\n"
"\t            $5000.00   Tor\n"
"\t- Misc\n"
"\t            $3775.00   reverse reversal\n"
"\t--------------------\n"
"\t          $152835.83\n"
"\t               71.67Ƀ\n"
"\t            82953.69€\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\tExpenses:\n"
"\t- Infrastructure\n"
"\t           $-7856.38   hardware\n"
"\t             -897.84€  hardware\n"
"\t             -630.00€  Internet hosting\n"
"\t            $-160.00   SSL certificates\n"
"\t- Travel & meetings\n"
"\t            -4907.08€  travel\n"
"\t            -3724.35€  hosting\n"
"\t            -4112.27€  food and drinks\n"
"\t            $-400.00   guests\n"
"\t             -330.30€  guests\n"
"\t- Subcontracting\n"
"\t          $-53414.00   subcontracting\n"
"\t           -20131.85€  subcontracting\n"
"\t- Running costs\n"
"\t            $-568.53   banking\n"
"\t             -174.53€  banking\n"
"\t           $-3707.33   administration\n"
"\t            -5439.61€  administration\n"
"\t- Misc\n"
"\t              -65.00€  stickers\n"
"\t             -414.17€  non stored purchases\n"
"\t              -20.00€  communication\n"
"\t--------------------\n"
"\t          $-66106.24\n"
"\t           -40847.00€\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\tTotal:\n"
"\t--------------------\n"
"\t           $86729.59\n"
"\t               71.67Ƀ\n"
"\t            42106.69€\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Income statement for 2013\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\tRevenues:\n"
"\t           $21000.00   NDI\n"
"\t           $20000.00   Tor (bounties program)\n"
"\t               29.58Ƀ  bitcoin\n"
"\t              330.00€  tax\n"
"\t--------------------\n"
"\t           $41000.00\n"
"\t               29.58Ƀ\n"
"\t              330.00€\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\tExpenses:\n"
"\t              -60.00€  banking\n"
"\t          $-17000.00   bounties\n"
"\t           $-1194.69   hardware\n"
"\t             -499.65€  hardware\n"
"\t             -390.00€  hosting\n"
"\t            -2341.00€  meeting\n"
"\t          $-21000.00   work\n"
"\t--------------------\n"
"\t          $-39194.69\n"
"\t            -3290.65€\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\tTotal:\n"
"\t--------------------\n"
"\t            $1805.31\n"
"\t               29.58Ƀ\n"
"\t            -2960.65€\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Income statement for 2012\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\tRevenues:\n"
"\t              137.30€  tax\n"
"\t--------------------\n"
"\t              137.30€\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\tExpenses:\n"
"\t           $-3810.71   hardware\n"
"\t             -856.79€  hardware\n"
"\t             -300.00€  hosting\n"
"\t            -3128.39€  meeting\n"
"\t            $-479.00   SSL certificates\n"
"\t--------------------\n"
"\t           $-4289.71\n"
"\t            -4285.18€\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\tTotal:\n"
"\t--------------------\n"
"\t           $-4289.71\n"
"\t            -4147.88€\n"
msgstr ""

#. type: Plain text
msgid ""
"In addition, one developer was paid full-time by the Tor Project, and NDI "
"sponsored 40 days of development work."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Income statement for 2011\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\tRevenues:\n"
"\t             7500.00€  Tor\n"
"\t--------------------\n"
"\t             7500.00€\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\tExpenses:\n"
"\t            $-555.00   hardware\n"
"\t            -1075.00€  hosting\n"
"\t            -3163.32€  meeting\n"
"\t--------------------\n"
"\t            $-555.00\n"
"\t            -4238.32€\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\tTotal:\n"
"\t--------------------\n"
"\t            $-555.00\n"
"\t             3261.68€\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Income statement for 2010\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\tRevenues:\n"
"\t            $8500.00   Tor\n"
"\t--------------------\n"
"\t            $8500.00\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\tExpenses:\n"
"\t            -2025.00€  hardware\n"
"\t--------------------\n"
"\t            -2025.00€\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\tTotal:\n"
"\t--------------------\n"
"\t            $8500.00\n"
"\t            -2025.00€\n"
msgstr ""
