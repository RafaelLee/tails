# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2018-01-30 12:41+0000\n"
"PO-Revision-Date: 2018-07-02 05:53+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: Arabic "
"<http://translate.tails.boum.org/projects/tails/unsafe_browser/ar/>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Weblate 2.10.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Logging in to captive portals\"]]\n"
msgstr ""

#. type: Plain text
msgid ""
"Many publicly accessible Internet connections (usually available through a "
"wireless network connection) require its users to register and login in "
"order to get access to the Internet. This include both free and paid for "
"services that may be found at Internet cafés, libraries, airports, hotels, "
"universities etc. Normally in these situations, a so called *captive portal* "
"intercepts any website request made and redirects the web browser to a login "
"page. None of that works when Tor is used, so a browser with unrestricted "
"network access is necessary."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Tails includes an <span class=\"application\">Unsafe Browser</span> for this\n"
"purpose, and it can be started via the menu\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"  <span class=\"guisubmenu\">Internet</span>&nbsp;▸\n"
"  <span class=\"guimenuitem\">Unsafe Web Browser</span></span>.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"The <span class=\"application\">Unsafe Browser</span> has a red theme to\n"
"differentiate it from [[<span class=\"application\">Tor Browser</span>|Tor_Browser]].\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p><strong>The <span class=\"application\">Unsafe Browser</span> is not\n"
"anonymous</strong>. Use it only to log in to captive portals or to\n"
"[[browse web pages on the local network|advanced_topics/lan#browser]].</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"note\">\n"
msgstr "<div class=\"note\">\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/anonymous_internet/unsafe_browser/chroot.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr ""
"[[!inline pages=\"doc/anonymous_internet/unsafe_browser/chroot.inline.ar\" "
"raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
msgid "Security recommendations:"
msgstr ""

#. type: Bullet: '* '
msgid ""
"Do not run this browser at the same time as the anonymous [[<span class="
"\"application\">Tor Browser</span>|Tor_Browser]]. This makes it easy to not "
"mistake one browser for the other, which could have catastrophic "
"consequences."
msgstr ""
