# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2016-07-14 12:47+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Bullet: '  - '
msgid "[[!traillink System_requirements|about/requirements]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[!traillink Warnings!|about/warning]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[!traillink Features_and_included_software|about/features]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[!traillink Why_does_Tails_use_Tor?|about/tor]]"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[!traillink Can_I_hide_the_fact_that_I_am_using_Tails?|about/fingerprint]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[!traillink Trusting_Tails|about/trust]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[!traillink License_and_source_code_distribution|about/license]]"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[!traillink Acknowledgments_and_similar_projects|about/"
"acknowledgments_and_similar_projects]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[!traillink Finances|about/finances]]"
msgstr ""
